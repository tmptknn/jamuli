
import * as THREE from './three.module.js';
import { Font , FontLoader} from './FontLoader.js';
import { TextGeometry } from './TextGeometry.js';

const wordListNames = [];
let wordListName = "suomi 5";

let words ={};

let wordList = null;
let wordTree = null;
let originalWordTree = null;
let wordTreeLeft = null;
let wordLength =5;
let statusElem = null;
let guessElem = null;
let guessesDiv = null;
let guessesLeft = null;
let word = "testi";
let guesses =[];
let timer = null;
let hardMode = false;
let won = false;
let localValues = null;
let streak = 0;
let visualisation = true;
const currentVersion = 2;


let trunk = null;
let scene = null;
let letterMeshes = [];
let letterMeshes2 = [];
let camera = null;
let renderer = null;
let step = 0.01;
let rotationChange = 0;
let originalRotation = 0;
let originalX = 0;
let animTrunk={};
let growth = 0;
let canvas = null;


const letters ="åyxwzcfgbdöjvymhäpnrleostukia".toUpperCase(); //"qwertyuiopåasdfghjklöäzxcvbnm"
            //"qwertyuiopåasdfghjklöäzxcvbnm"
            //"aikutsoelrnpähmyvjödbgfczwxyå"
function markGreen(str){
    return "<mark class=\"green\">"+str+"</mark>";
}

function markYellow(str){
    return "<mark class=\"yellow\">"+str+"</mark>";
}

function markWhite(str){
    return "<mark class=\"white\">"+str+"</mark>";
}

function markBlack(str){
    return "<mark class=\"black\">"+str+"</mark>";
}

function setStatus(str){
    statusElem.innerHTML = str;
    clearTimeout(timer);
    timer = setTimeout(()=>{ statusElem.innerHTML = " "; },10000);
}

function addWordRecursive(wordTreeRoot,word,index, wordLength){
    if(index>=wordLength) return;
    if(index ==wordLength-1){
        wordTreeRoot[word[index]] = word;
        return;
    }
    if(wordTreeRoot[word[index]] === undefined){
        wordTreeRoot[word[index]] = {}
    }
    addWordRecursive(wordTreeRoot[word[index]],word,index+1, wordLength);
}

function addWord(wordTree,word, wordLength){
    addWordRecursive(wordTree,word,0, wordLength);
}

function findRandomWordRecursive(tree, depth){
    if(depth >= wordLength) return tree;
    let count =0;
    for(let i=0; i< letters.length; i++){
        const letter = letters[i];
        if(tree[letter] !== undefined){
            count +=1;
        }
    }
    const random = Math.floor(Math.random()*count);
    count = 0;
    for(let i=0; i< letters.length; i++){
        const letter = letters[i];
        if(tree[letter] !== undefined){
            if(random ==count){
                return  findRandomWordRecursive(tree[letter], depth+1);
            }
            count+=1;
        }
    }
}

function findRandomWord(tree){
    return findRandomWordRecursive(tree,0);
}

function checkWordForLegalCharacters(word){
    let broken = false;
    for(let j=0; j< word.length; j++){
        const letter = word[j];
        if(letters.search(letter)==-1){
            broken = true;
            break;
        }
    }
    return !broken;
}

function makeWordTree(wordlist, wordLength){
    const wordTree ={};
    let words=0;
    for(let i = 0; i< wordlist.length; i++){
        let word = wordlist[i];
        if(word.length == wordLength && checkWordForLegalCharacters(word)){
            words++;
            addWord(wordTree,word, wordLength);
        }else{
            wordlist.splice(i,1);
            i--;
        }
    }
    console.log("Game has "+words+" words")
    return wordTree;
}

function newWord(){
    won = false;
    
    wordList = words[wordListName]['wordList'];
    wordTree = words[wordListName]['wordTree'];
    originalWordTree = words[wordListName]['originalWordTree'];
    wordLength = words[wordListName]['info']['wordLength'];
    wordTreeLeft = JSON.parse(JSON.stringify(wordTree));
    guesses = [];
    guessesLeft.innerHTML = "Sinulla on "+(6-guesses.length)+" arvausta jäljellä. Puussa on jäljellä "+recursivelyCountTree(wordTreeLeft,0)+" mahdollista sanaa.";
    word = wordList[Math.floor(Math.random()*(wordList.length-1))];
    console.log(word);  
    updateGuessesList();
    updateTree(scene, wordTreeLeft);
}

function checkGuess(guess){
    if(guess["guess"] == word){  //move this somewhere
        setStatus("Oikea sana onneksi olkoon!");
        winWithWord(word);
        for( let i=0; i<wordLength; i++){
            guess["results"][i]["correct"]=true;
            guess["wordResults"][i]["correct"]=true;
        }
    }

    let missing = [];
    for(let i=0; i<wordLength; i++){
        if(guess["guess"][i] == word[i]){
            guess["results"][i]["correct"] = true;
            guess["wordResults"][i]["correct"] = true;
        }else{
            missing.push({"letter":word[i], "position": i});
        }
    }

    for(let i=0; i<wordLength; i++){
        const letter = guess["guess"][i];
        if(letter != word[i]){
            const  isLetter = (element) => element["letter"] == letter && element["position"] != i;
            const index = missing.findIndex(isLetter);
            if(index !== -1){
                guess["results"][i]["inWord"] = true;
                guess["wordResults"][i]["inWord"] = true;
                missing.splice(index,1);
            }
            else{
                guess["results"][i]["notInWord"] = true;
                guess["wordResults"][i]["notInWord"] = true;
            }
        }else{
            guess["results"][i]["notInWord"] = true;
                guess["wordResults"][i]["notInWord"] = true;
        }   
    }
}

function checkGuesses(guesses){
    for(let i=0; i< guesses.length; i++){
        const guess = guesses[i];
        checkGuess(guess);
    }
}

function makeGuessElement(guess, letterResults){

    let str = "";
    for(let i=0; i<wordLength; i++){
        const letter = guess["guess"][i];
        if(guess["results"][i]["correct"]){
            str+=markGreen(letter);
            letterResults[letter]["correct"] = true;
        } else if(guess["results"][i]["inWord"] ){
            str+=markYellow(letter);
            if(!letterResults[letter]["correct"]){
                letterResults[letter]["inWord"] = true;
            }
        } else {
            str+=markBlack(letter);
            if(!letterResults[letter]["correct"] && !letterResults[letter]["inWord"]){
                letterResults[letter]["notInWord"] = true;
            }
        }
    }
    return str;
}

function updateGuessesList() {
    guessesDiv.innerHTML = "";
    const letterResults ={};
    for(let i=0; i<letters.length; i++){
        letterResults[letters[i]] = {"letter":letters[i], "correct":false, "inWord":false, "notInWord":false};
    }

    for(let i=0; i < guesses.length; i++){
        const guess = guesses[i];
        let guessP = document.createElement("p");
        guessP.innerHTML = makeGuessElement(guess, letterResults);
        guessesDiv.appendChild(guessP);
    }

    guessesDiv.appendChild(document.createElement("br"));
    const letterP = document.createElement("p");
    let str="";
    for(let i=0; i<letters.length; i++){
        const letter = letters[i];
        if(letterResults[letter]["correct"]){
            str += markGreen(letter);
        }
        else if(letterResults[letter]["inWord"]){
            str += markYellow(letter);
        }
        else if(letterResults[letter]["notInWord"]){
            str += markBlack(letter);
        }
        else{
            str += markWhite(letter);
        }
    }
    letterP.innerHTML = str;
    if(guesses.length == 0){
        const helpP=document.createElement("p");
        helpP.innerHTML ="Yritä arvata "+wordLength+" kirjaiminen sana kuudella yrityksellä. Oikeat kirjaimet näytetään "+markGreen("vihreällä")+" ja oikeat kirjaimet väärässä paikassa "+markYellow("keltaisella")+". Kirjaimet arvausten alla näyttävät myös kirjaimet oikeissa paikoissa "+markGreen("vihreällä")+" ja kirjaimet väärissä paikoissa "+markYellow("keltaisella")+". Kirjaimet joita et ole vielä kokeillut ovat merkitty "+markWhite("valkealla")+" ja sanasta täysin puuttuvat kirjaimet "+markBlack("mustalla")+".";
        //"Try to guess 5 letter word. Correct letters are shown "+markGreen("green")+" and correct letters on wrong place "+markYellow("yellow")+". Letters below guesses also show letters in word put at least once in correct place as"+markGreen("green")+" and at least once correct letter on wrong place "+markYellow("yellow")+". Letters you have not tried yet are shown "+markWhite("white")+" and letters not in word at all "+markBlack("black")+".";
        guessesDiv.appendChild(helpP);
    }
    const unusedLetters =
    guessesDiv.appendChild(letterP);
    
}

function doesWordTreeHas(wordTree,guess){
    let wordTreeRoot = wordTree;
    for(let i=0; i< wordLength; i++){
        wordTreeRoot = wordTreeRoot[guess[i]];
        if(wordTreeRoot ===undefined){
            return false;
        }
    }
    return true;
}

function calcMatches(correct, guess){
    let match=0;
    let close=0;
    let miss =wordLength;
    for(let i=0; i<wordLength; i++){
        if(correct[i]==1){
            match+=1;
            miss-=1;
        }
        if(correct[i]==2){
            close+=1;
            miss-=1;
        }
    }
    return { match, close, miss};
}

function skip(correct, guesses){
    for(let i=0; i< guesses.length; i++){
        const result = guesses[i]["wordResults"];
        let count = 0;
        for(let j=0; j< wordLength; j++){
            if(guesses.length <2 && correct[j] == 1){
               return count++;
            }
        }
        if(count>1) return true;
    }
    return false;
}

function generateCombinations(guesses){
    const helper ="";
    for(let n=0; n<guesses.length;n++){
        const corrects = [];
        function Generate(helper,limit)
        {
            if (helper.length == limit){
                const correct =[]
                for(let i=0; i<limit; i++){
                    correct.push(parseInt(helper[i]));
                }
                if(!skip(correct, guesses,n)){
                    corrects.push(correct);
                }
            }
            else
            {
                Generate(helper+0, limit);
                Generate(helper+1, limit);
                Generate(helper+2, limit);
            }
        }

        Generate(helper,wordLength);
        guesses[n]["corrects"] = corrects;
    }

}

function recursivelyCountTree(tree, index){
    if(index>=wordLength) return 1;
    let sum = 0;
    for(let i=0; i< letters.length; i++){
        if(tree[letters[i]] !== undefined ){
            sum+=recursivelyCountTree(tree[letters[i]],index+1);
        }
     }
     return sum;
    
}

function doesWordMatchMask(word,guesses, correctOne){
    
    for(let j=0; j< guesses.length; j++){
        const guess = guesses[j]["guess"];
        const correct = (j==guesses.length-1)?correctOne:guesses[j]["correct"]

        let missing = [];
        for(let i=0; i< wordLength; i++){
            if(correct[i] !=1 && guess[i] == word[i]){
                return false;
            }
            if(correct[i] == 1){
                if(guess[i]!=word[i]){
                    return false;
                }
            }else{
                missing.push({"letter":word[i], "position":i});
            }
        }
        let missingAtStart = 0;

        const missingX = [];
        for(let i=0; i< correct.length; i++){
            if(correct[i]==2){
                 missingAtStart+=1;
                 missingX.push({"letter":guess[i], "position":i})
            }
        }
        let found =0;
        

        for(let i=0; i< wordLength; i++){
            if(correct[i] != 1){ 
                const letter =guess[i];
                const ind = i;
                const find = element => element["letter"] == letter && element["position"] != ind ;
                const index = missing.findIndex(find);
                if(index !=-1){
                    found+=1;
                    missing.splice(index,1);
                }
            }
        }
        if(missingAtStart != found){
            return false;
        }

        for(let i=0; i<wordLength; i++){
            for(let j=0; j<wordLength; j++){
                if(correct[i] ==0 && guess[i] == word[j] && correct[j] !=1 ){
                    const letter= word[j];
                    const find = element => element["letter"] == letter;
                    const index = missingX.findIndex(find);
                    if(index ==-1){
                        return false;
                    }
                }
            }
        }
    }
    return true;

}

function doesWordMatchLastMask(currentWord,guesses, correct){
    const guess = guesses[guesses.length-1]["guess"];
    let missing = [];
    const used = [];
    for(let i=0; i< wordLength; i++){
        used.push(0);
        if(correct[i] !=1 && guess[i] == currentWord[i]){
            return false;
        }
        if(correct[i] == 1){
            if(guess[i]!=currentWord[i]){
                return false;
            }
        }else{
            missing.push({"letter":guess[i], "position":i});
        }
    }

    for(let i=0; i<wordLength; i++){
        if(correct[i] ==2){
            let found = false;
            for(let j=0;j<wordLength; j++){
                if(guess[i] == currentWord[j] && correct[j]!=1 &&j!=i && used[j]==0){
                    found =true;
                    used[j] =1;
                    break;
                }
            }
            if(!found){
                return false;
            }
        }
    }

    for(let i=0; i<wordLength; i++){
        for(let j=0; j<wordLength; j++){
            if(correct[i] ==0 && guess[i] == currentWord[j] && currentWord[j] != word[j]){
                return false;
            }
        }
    }
    
    return true;

}


function recursivelyPruneTree(tree, index, guesses, correct){
    if(index>=wordLength){
       return doesWordMatchMask(tree, guesses, correct);
    }
    let found =false;
    for(let i=0; i< letters.length; i++){
        if(tree[letters[i]] !== undefined ){
            if(recursivelyPruneTree(tree[letters[i]],index+1,guesses, correct)){
                found = true;
            }else{
                tree[letters[i]] = undefined;
            }
        }
    }
    return found;    
}

function recursivelyPruneTreeWithLastMask(tree, index, guesses, correct){
    if(index>=wordLength){
       return doesWordMatchLastMask(tree, guesses, correct);
    }
    let found =false;
    for(let i=0; i< letters.length; i++){
        if(tree[letters[i]] !== undefined ){
            if(recursivelyPruneTreeWithLastMask(tree[letters[i]],index+1,guesses, correct)){
                found = true;
            }else{
                tree[letters[i]] = undefined;
            }
        }
    }
    return found;    
}


function findLargestNotCorrectTree(wordTreeLeft, guesses){  // this function is too big
    const weightTreeOriginal = JSON.parse(JSON.stringify(wordTreeLeft));
    let minCount = 0;
    let resultCorrect = [0,0,0,0,0];
    let minMatches = {"match":wordLength+1, "close":wordLength+1, "miss":-1};
    let resultTree = JSON.parse(JSON.stringify(weightTreeOriginal));
    
    generateCombinations(guesses);

    for(let j=0; j<guesses[guesses.length-1]["corrects"].length; j++){
        const correct = guesses[guesses.length-1]["corrects"][j];
        const matches = calcMatches(correct, guess);
        const weightTree = JSON.parse(JSON.stringify(weightTreeOriginal));
        recursivelyPruneTree(weightTree, 0, guesses, correct);
        const count = recursivelyCountTree(weightTree,0);

        const countIsGood =count > 0 && count>minCount;
        const countIsSame = count >0 && count>=minCount;
        const correctMatchesIsBetter = matches["correct"]<minMatches["correct"];
        const matchesAreBetterOrSame = correctMatchesIsBetter && matches["close"]<= matches["close"];
        const closeMatchesIsBetter =matches["close"]< matches["close"];
        const matchesAreBetter = matchesAreBetterOrSame && ( correctMatchesIsBetter|| (correctMatchesIsBetter && closeMatchesIsBetter));
        const notCorrectMask = !(correct[0]==1 &&correct[1]==1 &&correct[2]==1 &&correct[3]==1 &&correct[4]==1);
        if(countIsGood || (countIsSame && matchesAreBetter)||
        (countIsSame && Math.random()<0.5 && matchesAreBetterOrSame)){ //this is not even. But do we want to save multiple trees?
            console.log("Count "+count+ " match "+matches["match"]+" close "+matches["close"]+" "+correct);
            if(notCorrectMask||count==1){
                minCount = count;
                resultTree = JSON.parse(JSON.stringify(weightTree));
                resultCorrect = JSON.parse(JSON.stringify(correct));
                minMatches = JSON.parse(JSON.stringify(matches));
            }    
        }
    }                 

    console.log("MinCount "+minCount + " min matches "+minMatches["match"] +" min close "+minMatches["close"]+" "+resultCorrect);
    if(guesses.length>0) guesses[guesses.length-1]["correct"] = resultCorrect;
    const guessItem = guesses[guesses.length-1]["results"];
    const wordcount =recursivelyCountTree(resultTree, 0);
    const hasWord = doesWordTreeHas(resultTree, guesses[guesses.length-1]["guess"]);
    console.log("Words left "+wordcount + " tree has guess "+hasWord);
    guesses[guesses.length-1]["hasWord"] = hasWord;
    guesses[guesses.length-1]["wordResults"] = JSON.parse(JSON.stringify(guesses[guesses.length-1]["results"]));
    const wordResults =guesses[guesses.length-1]["wordResults"];

    if(((wordcount<2 && hasWord)||wordcount == 0)){
        for(let i=0; i< wordLength; i++){
            guessItem[i]["correct"] = true;
            wordResults[i]["correct"] = true
            
        }
        setStatus("Onnittelut voitit vaikean pelin!");
        winWithWord(guesses[guesses.length-1]["guess"]);
        
        return resultTree;
    }

    for(let i=0; i< wordLength; i++){
        if(resultCorrect[i]==1 ){
            guessItem[i]["correct"] = true;
        }
        if(resultCorrect[i]==2 && guessItem[i]["correct"] == false ){
            guessItem[i]["inWord"] = true;
        }
        if(resultCorrect[i] ==0 && guessItem[i]["inWord"]==false && guessItem[i]["correct"] == false){
            guessItem[i]["notInWord"] =true;

        }
    }
    return resultTree;
}


function removeFromTree(tree, word, index){
    if(tree === undefined) return false;
    if(tree == word) return true;
    if(removeFromTree(tree[word[index]], word, index+1)){
        tree[word[index]] = undefined;
        for(let j=0; j< letters.length; j++){
            if(tree[letters[j]] != undefined){
                return false;
            }
        }
        return true;
    }
    return false;
}

function addWordToExclusionList(word){
    const find = element => element ==word;
    const index = words[wordListName]["exclusion"].findIndex(find);
    if(index ==-1){
        words[wordListName]["exclusion"].push(word);
    }
    saveLocalStorage();
}

function winWithWord(word){
    if(!won){
        guessesLeft.innerHTML = "Onneksi olkoon arvasit sanan! Se oli "+word+". Valitse uusi sana ja koita saada mahdollisimman monta peräkkäin oikein.";
        const test = (element) => element == word;
        const index = wordList.findIndex(test);
        if(index !=-1){
            wordList.splice(index,1);
        }
        increaseStreak();
        removeFromTree(wordTree,word,0);
        addWordToExclusionList(word);
    }
    won =true;
}

function lastGuessResult(guesses){
    const correct = [];
    const guess = guesses[guesses.length-1];
    for(let i=0; i< wordLength; i++){
        if(guess["results"][i]["correct"]){
            correct.push(1);
        }else if(guess["results"][i]["inWord"]) {
            correct.push(2);
        }else{
            correct.push(0);
        }
    }
    return correct;
}

function findLargestNotCorrectTreeWWithLastGuess(wordTreeLeft, guesses){
    const correct = lastGuessResult(guesses);
    recursivelyPruneTreeWithLastMask(wordTreeLeft, 0, guesses, correct);
    const count = recursivelyCountTree(wordTreeLeft,0);
    console.log("tree has "+count+ " words left.")
    return wordTreeLeft;
}

function pruneWordTree(guesses){
    wordTreeLeft =findLargestNotCorrectTree(wordTreeLeft, guesses);
    if(wordTreeLeft) console.log(JSON.stringify(wordTreeLeft));
    updateTree(scene, wordTreeLeft);
    return wordTreeLeft;
}

function pruneWordTreeWithLastGuess(guesses){
    wordTreeLeft =findLargestNotCorrectTreeWWithLastGuess(wordTreeLeft, guesses);
    if(wordTreeLeft) console.log(JSON.stringify(wordTreeLeft));
    updateTree(scene, wordTreeLeft);
    return wordTreeLeft;
}


function makeGuesses(guesses){
    pruneWordTree(guesses);
}

function makeFalseGuesses(guesses){
    pruneWordTreeWithLastGuess(guesses);
}

function makeGuess(){
    if(won){
        newWord();
    }
    if(guesses.length>=6){
        setStatus("Hävisit jo pelin. Aloita uusi");
        guessElem.value = "";
        return
    }
    let guess = guessElem.value.toUpperCase();
    if (guess[wordLength-1] === '\n'){
        guess.splice(wordLength-1,1);
    }

    if(guess.length != wordLength){
        setStatus("Sanan on oltava "+wordLength+" kirjainta");
        guessElem.value = "";
        return;
    }

    if(!checkWordForLegalCharacters(word)){
        setStatus("Sanassa on laittomia merkkejä");
        guessElem.value = "";
        return;
    }
    
    if(doesWordTreeHas(originalWordTree, guess)){
        console.log("Made guess "+guess);
        setStatus("Arvasit "+guess);
        const results = []
        for(let i=0; i< wordLength; i++){
            results.push({"letter":guess[i],"correct":false, "inWord":false, "notInWord":true});
        }
        const wordResults = JSON.parse(JSON.stringify(results));
        guesses.push({"guess":guess,"results":results, "wordResults":wordResults});
        guessesLeft.innerHTML = "Sinulla on "+(6-guesses.length)+" arvausta jäljellä.";
        if(hardMode){
            makeGuesses(guesses);
        }else {
            checkGuesses(guesses);
            makeFalseGuesses(guesses);
        }
        updateGuessesList();
        guessElem.value = "";
        if(guesses.length>=6 && !won){
            if(hardMode) word = findRandomWord(wordTreeLeft);
            setStatus("Hävisit pelin. Oikea sana olisi ollut "+word);
            lossStreak();
            guessesLeft.innerHTML= "Hävisit pelin.Oikea sana olisi ollut "+word+". Valitse uusi sana!"
            guessElem.value = "";
        }
    }else{
        /*
        const index =words[wordListName]["exclusion"].findIndex(find);
        if(index !=-1){
            setStatus("Olet arvannut jo aiemmassa pelissä sanan "+guess);
        }else{
            */
            setStatus("Peli ei tunne sanaa "+guess);
        //}
        guessElem.value = "";
    }
}

function changeFavicon(src) {
    var link = document.createElement('link'),
        oldLink = document.getElementById('dynamic-favicon');
    link.id = 'dynamic-favicon';
    link.rel = 'shortcut icon';
    link.href = src;
    if (oldLink) {
     document.head.removeChild(oldLink);
    }
    document.head.appendChild(link);
   }

function init(){
    //changeFavicon("./pics/favicon.ico");

    document.getElementById("newWord").onclick = () => {
        if(!won){
            lossStreak();
        }
        newWord();
    }
    document.getElementById("guess").onclick = makeGuess;
    statusElem = document.getElementById("status");
    guessElem = document.getElementById("guessText");
    guessesDiv = document.getElementById("guesses");
    guessesLeft = document.getElementById("guessesLeft");
    const visualisationCheckBox =document.getElementById("visualisationCheckBox");
    visualisationCheckBox.checked = visualisation;
    visualisationCheckBox.onclick = () => {
        visualisation = visualisationCheckBox.checked;
        saveLocalStorage();
        updateTree(scene,wordTreeLeft);
    };

    const hardModeCheckbox = document.getElementById("hardMode");

    hardModeCheckbox.checked =hardMode;
    hardModeCheckbox.onclick = () => {
        hardMode = hardModeCheckbox.checked;
        saveLocalStorage();
        newWord();
    };
    guessElem.onkeypress = function(e){
        if (!e) e = window.event;
        var keyCode = e.code || e.key;
        if (keyCode == "Enter"){
          makeGuess();
          return false;
        }
      }
    const wordListSelect = document.getElementById("wordListSelect");
    for(let i=0; i<wordListNames.length; i++){
        const name = wordListNames[i];
        const option =document.createElement("option");
        if(name == wordListName){
            option.selected = true;
        }
        option.value = name;
        option.text = name;
        wordListSelect.appendChild(option);
    }

    
    updateStreak();


    wordListSelect.onchange = (x,y) =>{
        wordListName = wordListSelect.value;
        saveLocalStorage();
        newWord();
    };
}

function increaseStreak(){
    streak+=1;
    if(words[wordListName]["streak"] === undefined)
    {
        words[wordListName]["streak"] = 0;
    }

    words[wordListName]["streak"] +=1;
    updateStreak();
}

function lossStreak(){
    streak = 0;
    words[wordListName]["streak"] = 0;
    updateStreak();
    saveLocalStorage()
}

function updateStreak(){
    if(words[wordListName]["streak"] === undefined)
    {
        words[wordListName]["streak"] = 0;
    }
    document.getElementById("streak").innerHTML = "Olet saanut putkeen kaikkiaan "+streak+" oikein! Tällä sanalistalla "+words[wordListName]["streak"]+" putkeen oikein!";
}

function loadLocalStorage(){
    const value =localStorage.getItem("Jamuli");
    if(value){
        localValues = JSON.parse(value);
        if(localValues.version !== currentVersion){
            localValues = { "wordLists":{}, "wordListNames":[], "hardMode":hardMode };
            return;
        }
        for(let i=0; i<localValues["wordListNames"].length;i++){
            const name =  localValues["wordListNames"][i];
            if(!words[name]){
                words[name] = {};
            }
            words[name]["exclusion"] = localValues["wordLists"][name]["exclusion"];
            if(localValues["wordLists"][name].streak !== undefined){
                words[name]["streak"] = localValues["wordLists"][name].streak;
            }
        }
        if(localValues.hardMode !== undefined){
            hardMode = localValues.hardMode;
        }
        if(localValues.wordListName !== undefined){
            wordListName = localValues.wordListName;
        }
        if(localValues.streak !== undefined)
        {
            streak = localValues.streak;
            updateStreak();
        }
        if(localValues.visualisation !== undefined)
        {
            visualisation = localValues.visualisation;
        }
        
    }else{
        localValues = { "wordLists":{}, "wordListNames":[], "hardMode":hardMode };
    }
}

function saveLocalStorage(){
    for(let i=0; i< wordListNames.length; i++){
        const name = wordListNames[i];
        localValues["wordListNames"] = wordListNames;
        if(!localValues["wordLists"]){
            localValues["wordLists"] = {};
        }
        if(!localValues["wordLists"][name]){
            localValues["wordLists"][name] = {};
        }
        localValues["wordLists"][name]["exclusion"] = words[name]["exclusion"];
        localValues["wordLists"][name]["streak"] = words[name]["streak"];
    }
    localValues.hardMode=hardMode;
    localValues.wordListName = wordListName;
    localValues.streak = streak;
    localValues.visualisation = visualisation;
    localValues.version = currentVersion;
    localStorage.setItem("Jamuli",JSON.stringify(localValues));
}

function loadWordFile(info){
    return new Promise( function(resolve,reject){
    fetch(info["filename"])
                .then(response => response.text())
                .then(text => {
                    const wordList = text.toUpperCase().split('\n');
                    
                    const originalWordTree = makeWordTree(wordList, info['wordLength']);
                    const wordTree = JSON.parse(JSON.stringify(originalWordTree));
                    const exclusion = (words[info["name"]] && words[info["name"]]["exlusion"])?words[info["name"]]["exclusion"]:[];
                    for(let i=0; i< exclusion.length; i++){
                        const word = exclusion[i];
                        const test = (element) => element == word;
                        const index = wordList.findIndex(test);
                        if(index !=-1){
                            wordList.splice(index,1);
                        }
                    }
                    wordListNames.push(info["name"]);
                    
                    words[info.name] = {wordList, wordTree, originalWordTree, info, exclusion};
            
                }
                ).then(()=>{
                    resolve();
                });
            });
}

function loadWords(){

    const promises = [
        {'name':'suomi 3','language':'finnish','wordLength':3,'filename':'./words/kotus3letterlist.txt'},
        {'name':'suomi 4','language':'finnish','wordLength':4,'filename':'./words/kotus4letterlist.txt'},
        {'name':'suomi 5','language':'finnish','wordLength':5,'filename':'./words/kotus5letterlist.txt'},
        {'name':'suomi 6','language':'finnish','wordLength':6,'filename':'./words/kotus6letterlist.txt'},
        {'name':'suomi 7','language':'finnish','wordLength':7,'filename':'./words/kotus7letterlist.txt'},
        {'name':'englanti 5 10k','language':'english','wordLength':5,'filename':'./words/10kenglish5letterlist.txt'},
        {'name':'englanti 6 10k','language':'english','wordLength':6,'filename':'./words/10kenglish6letterlist.txt'},
        {'name':'englanti 5 20k','language':'english','wordLength':5,'filename':'./words/20kenglish5letterlist.txt'},
        {'name':'englanti 6 20k','language':'english','wordLength':6,'filename':'./words/20kenglish6letterlist.txt'},
        {'name':'englanti 5 30k','language':'english','wordLength':5,'filename':'./words/30kenglish5letterlist.txt'},
        {'name':'englanti 6 30k','language':'english','wordLength':6,'filename':'./words/30kenglish6letterlist.txt'}
    ].map(loadWordFile);

    loadLocalStorage();
    const loader = new FontLoader();
    loader.load( './js/arial.json', function ( font ) {
        makevis();
        for(let i=0; i<letters.length; i++){
            const letter = letters[i];
            const geometry = new TextGeometry( letter, {
                font: font,
                size: 0.30,
                height: 0.01,
                curveSegments: 3,
                bevelEnabled: false,
                bevelThickness: 0.001,
                bevelSize: 0.001,
                bevelOffset: 0,
                bevelSegments: 2
            } );
            const textMaterial = new THREE.MeshPhongMaterial( { color: 0x228822 } );
            const textMaterial2 = new THREE.MeshPhongMaterial( { color: 0x444422} );
            const mesh = new THREE.Mesh( geometry, textMaterial );
            const mesh2 = new THREE.Mesh( geometry, textMaterial2 );
            mesh.position.set( -0.15, 0.5, 0 );
            mesh2.position.set( -0.15, 0.5, 0 );
            letterMeshes.push(mesh);
            letterMeshes2.push(mesh2);
        }
        
        Promise.all(promises).then(() =>{
            init();
            newWord(); 
        })
       
    }); 
}

function onMouseDown(ev){
    if(trunk){
         originalRotation = trunk.rotation.y;
         originalX = ev.clientX;
         step = 0;
    }
}

function onMouseMove(ev){
    if(trunk && ev.buttons ==1){
        const diff = originalX- ev.clientX;
        rotationChange = Math.PI*2.5*-diff/window.innerWidth;
    }
}

function onMouseUp(ev){
    if(trunk){
        originalRotation = trunk.rotation.y;
        step = 0.01;
    }
}

    function makevis(){
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 1, 500 );
    

        document.body.onresize = (ev)=>{ 
            //camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 1, 500 );
            camera.aspect =window.innerWidth/window.innerHeight;
            camera.updateProjectionMatrix();
            renderer.setSize( window.innerWidth,window.innerHeight);
        }

        renderer = new THREE.WebGLRenderer();
        renderer.setSize( window.innerWidth,window.innerHeight);
        const vis = document.getElementById("visualisation");
        vis.appendChild( renderer.domElement );
        canvas = renderer.domElement;
        vis.onmousedown = onMouseDown;
        vis.onmousemove = onMouseMove;
        vis.onmouseup = onMouseUp;
    
        const light = new THREE.AmbientLight( 0x404040 ); // soft white light
        scene.add( light );
        const directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5);
        scene.add( directionalLight );
        
        camera.position.set( 0, 0, 5.0);
        camera.lookAt( 0, 0, 0 );

        const  easeOutSine = (x) => Math.sin((x * Math.PI) / 2)

        const easeOutElastic = (x) => {
            const c4 = (2 * Math.PI) / 3;
            return x === 0
              ? 0
              : x === 1
              ? 1
              : Math.pow(2, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
            
        }

    
        function animate() {
            if(trunk){ 
                if(step ==0){
                    trunk.rotation.y = originalRotation+rotationChange;
                }
                trunk.rotation.y += step;
                const gr=easeOutSine(Math.min(Math.max(growth-0.5,0),1.0));
                const cscale = gr;
                trunk.scale.x =cscale;
                trunk.scale.y = cscale;
                trunk.scale.z = cscale;
            }

            growth =growth +0.01;
            const vowels ="EYUIOÅAÖÄ";
            const offset =0.0;

            

            function recAnim(animTrunk, depth){
                for(let i=0; i< letters.length; i++){
                    const letter = letters[i];
                    if(animTrunk[letter] !== undefined){
                        const animBranch = animTrunk[letter];
                        const current = animBranch["current"];
                        const startX = animBranch["startX"];
                        const endX = animBranch["endX"];
                        const startY = animBranch["startY"];
                        const mscale = animBranch["scale"];
                        const root = animBranch["root"];
                        current.rotation.y =startY;
                        current.rotation.x = startX;
                        current.rotation.z = 0;
                        const growl =Math.min(Math.max(growth-offset,0),1.0);
                        const gr=easeOutSine(growl);
                        const cscale = mscale*growl;
                        current.scale.x =cscale;
                        current.scale.y = cscale;
                        current.scale.z = cscale;
                        const letterMesh = animBranch["letterMesh"];
                        const lscale = easeOutSine(Math.min(Math.max(growth-1+offset,0),1.0));
                        letterMesh.scale.x=lscale;
                        letterMesh.scale.y=lscale;
                        letterMesh.scale.z=lscale;
                        const bowwow =easeOutElastic(growth);
                        const rotX = root?endX:endX*bowwow;
                        current.rotateX(rotX);
                        recAnim(animBranch,depth-1);
                    }
                }
            }

            recAnim(animTrunk,0,0);

            requestAnimationFrame( animate );
            renderer.render( scene, camera );
        }
        animate();
    }

    function startRecording() {
        const chunks = []; // here we will store our recorded media chunks (Blobs)
        const stream = canvas.captureStream(); // grab our canvas MediaStream
        const rec = new MediaRecorder(stream); // init the recorder
        // every time the recorder has new data, we will store it in our array
        rec.ondataavailable = e => chunks.push(e.data);
        // only when the recorder stops, we construct a complete Blob from all the chunks
        rec.onstop = e => exportVid(new Blob(chunks, {
           type: 'video/mp4'
        }));
     
        rec.start();
        setTimeout(() => rec.stop(), 10000); 
     }
     
     function exportVid(blob) {
        const vid = document.createElement('video');
        vid.src = URL.createObjectURL(blob);
        vid.controls = true;
        document.body.appendChild(vid);
        const a = document.createElement('a');
        a.download = 'game.mp4';
        a.href = vid.src;
        a.textContent = 'download the video';
        document.body.appendChild(a);
     }
    
    function updateTree(scene, wordTree){
        if(trunk != null){
            console.log("remove tree");
            scene.remove(trunk);
            animTrunk["current"] = undefined;
        }
        growth = 0;
        if(!visualisation) return;
    
        const lineMaterial = new THREE.MeshPhongMaterial( { color: 0x887755 } );
        const trunkGeometry = new THREE.ConeGeometry( /*0.025,*/ 0.04, 1.3, 11, 1, 0);
        trunkGeometry.applyMatrix4( new THREE.Matrix4().makeTranslation(0, 0.60, 0) );
        trunk = new THREE.Mesh( trunkGeometry, lineMaterial );
        const mainRoot = trunk.clone();
        mainRoot.rotateX(Math.PI);
        trunk.add(mainRoot);
        animTrunk["current"] = trunk;
        scene.add( trunk);
        const branchGeometry = new THREE.ConeGeometry( /*0.01875,*/ 0.020, 0.8, 7,1,0 );
        branchGeometry.applyMatrix4( new THREE.Matrix4().makeTranslation(0, 0.4, 0) );
        const scaler =0.80;
    
        const trunkColor = new THREE.Color(0x334422);
        const trunkColor2 = new THREE.Color(0x887755);
        const leafColor = new THREE.Color(0x227722);
        const rootColor = new THREE.Color(0x223311);
        const branches =[];
        const roots = [];
        for(let i=0;i<wordLength;i++){
            const branchMaterial = new THREE.MeshPhongMaterial( { color:new THREE.Color().lerpColors(trunkColor,leafColor, i/5.0) } );
            const branch = new THREE.Mesh( branchGeometry, branchMaterial);
            branches.push(branch);
            const rootMaterial = new THREE.MeshPhongMaterial( { color:new THREE.Color().lerpColors(trunkColor2,rootColor, i/5.0) } );
            const root = new THREE.Mesh( branchGeometry, rootMaterial);
            roots.push(root);
        }
        const vowels ="EYUIOÅAÖÄ";
        function recTree(treeRoot, mainbranch, depth, root, animRoot){
            
            let vowelCount =0;
            let consonantCount = 0;
            if(depth>=wordLength) return;
            for(let i=0; i< letters.length; i++){
                const letter = letters[i];
                if(treeRoot[letter] !== undefined){
                    const animBranch ={};
                    animRoot[letter] =animBranch;
                    const vowel =vowels.search(letter)!=-1;
                    const first = depth==0;
                    const firstVowel =vowel&&first;
                    root = first?firstVowel:root;
                    const branch = (root?roots[depth].clone():branches[depth].clone());
                    animBranch["current"] = branch;
                    branch.position.set(0,(firstVowel?-1:1)*(0.6-Math.random()*0.4),0);
                    branch.scale.set(scaler,scaler,scaler);
                    const startX = firstVowel?Math.PI:0;

                    let rotY=(Math.random()*0.1)+i*Math.PI*2.0/letters.length;
                    
                    if(first){
                        vowelCount+=(vowel)?1:0;
                        consonantCount+=(vowel)?0:1;
                        if(vowel){
                            rotY = vowelCount*Math.PI*2.0/vowels.length;
                        }else{
                            rotY =consonantCount*Math.PI*2.0/(letters.length-vowels.length);
                        }
                    } 
                    
                    const startY = rotY;
                    const endY =  rotY;
                    const endX = Math.random()*0.6+0.5+(root?depth*-0.4:0.0);

                    animBranch["startX"] = startX;
                    animBranch["endX"] = endX;
                    animBranch["startY"] = startY;
                    animBranch["endY"] = endY;
                    const letterMesh = root?letterMeshes2[i].clone():letterMeshes[i].clone();
                    animBranch["letterMesh"] = letterMesh;
                    animBranch["scale"] = branch.scale.x;
                    animBranch["root"] = root !== false;
                    branch.add(letterMesh);
                    letterMesh.scale.x=0;
                    letterMesh.scale.y=0;
                    letterMesh.scale.z=0;
                    mainbranch.add(branch);
                    recTree(treeRoot[letters[i]],branch, depth+1, root, animBranch);
                }
            }
        }
    
        recTree(wordTree, trunk,0,true, animTrunk);
        //startRecording();
    }

loadWords();